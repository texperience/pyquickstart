# pyquickstart

[![Pipeline][pipeline-badge]][pipeline-link]
[![Coverage][coverage-badge]][coverage-link]
[![PyPI][pypi-badge]][pypi-link]

[pipeline-badge]: https://gitlab.com/texperience/pyquickstart/badges/master/pipeline.svg
[pipeline-link]: https://gitlab.com/texperience/pyquickstart/pipelines
[coverage-badge]: https://gitlab.com/texperience/pyquickstart/badges/master/coverage.svg
[coverage-link]: https://gitlab.com/texperience/pyquickstart/-/jobs
[pypi-badge]: https://img.shields.io/pypi/v/pyquickstart.svg
[pypi-link]: https://pypi.python.org/pypi/pyquickstart

Create Python projects easily using well-defined templates.

## Features

* Intuitive Python project creation
* Well-defined project structure using source directory layout
* Pre-configured code linting with PEP8 in mind
* Improved code quality leveraging static code analysis
* Enabled unit and integration testing supporting test-driven development
* Production-ready continuous integration and deployment toolchain

## Standard template

We use this template for our own projects following modern best practices:

* Formatting code with brunette (a better configurable black fork)
* Sort and style import with isort
* Linting with flake8
* Testing automation with pytest and tox for current Python releases
* Measuring code coverage using coverage
* Continuous integration on Gitlab
* Packaging as wheel
* Deployment on PyPI using twine

## Technical requirements

Below is the list of currently supported Python releases:

| #   | Python |
|-----|--------|
| 1   | 3.7    |
| 2   | 3.8    |
| 3   | 3.9    |
| 4   | 3.10   |

## Code and contribution

The code is open source and released under the [MIT License (MIT)][mit-license]. It is available on [Gitlab][gitlab] and follows the guidelines about [Semantic Versioning][semver] for transparency within the release cycle and backward compatibility whenever possible.

All contributions are welcome, whether bug reports, reviews, documentation or feature requests.

If you're a developer and have time and ideas for code contributions, fork the repo and prepare a merge request:

```bash
# Prepare your environment the first time
python3.10 -m venv ~/virtualenvs/pyquickstart-py310
source ~/virtualenvs/pyquickstart-py38/bin/activate
pip install -e .[development]

# Running the tests while development
pytest

# Individual Python release tests and code quality checks
tox -e py310
tox -e brunette
tox -e coverage
tox -e flake8
tox -e isort
tox -e mypy

# Ensure code quality running the entire test suite,
# this requires all supported Python releases to be installed
tox
```

[mit-license]: https://en.wikipedia.org/wiki/MIT_License
[gitlab]: https://gitlab.com/texperience/pyquickstart
[semver]: http://semver.org/

## Installation

Install `pyquickstart` using `pip`:

```bash
pip install pyquickstart
```

## Usage

Create a new Python project from the standard template in your current working directory by running `pyquickstart`:

```bash
pyquickstart
```

Then answer a few simple questions, default values in square brackets:

```bash
project [shinyprojectname]: 
author [John Doe]: 
author_email [john@example.com]: 
repository_url [https://gitlab.com/johndoe/shinyprojectname]: 
year [2022]: 
```
