from setuptools import find_namespace_packages, setup

# Installation dependencies
install_requires = [
    'click>=8.0,<9.0',
    'cookiecutter>=1.7,<2.0',
]

# Development dependencies
development_extras = [
    # Pin brunette as long at is incompatible with blacks latest refactorings
    'brunette==0.2.0',
    'coverage>=6.3,<7.0',
    'flake8>=4.0,<5.0',
    'isort>=5.10,<6.0',
    'mypy>=0.931,<1.0',
    'pytest>=7.0,<8.0',
    'pytest-mock>=3.7,<4.0',
    'tox>=3.24,<4.0',
]

setup(
    packages=find_namespace_packages(exclude=['tests']),
    include_package_data=True,
    license='MIT License (MIT)',
    description='Create Python projects easily using well-defined templates.',
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
    url='https://gitlab.com/texperience/pyquickstart',
    author='Timo Rieber',
    author_email='trieber@texperience.de',
    install_requires=install_requires,
    extras_require={
        'development': development_extras,
    },
    entry_points={
        'console_scripts': ['pyquickstart=pyquickstart.cli:main'],
    },
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',
        'Topic :: Software Development',
    ],
)
