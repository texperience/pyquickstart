from click.testing import CliRunner

from pyquickstart.cli import main


def test_exit_code_on_success(mocker):
    mocker.patch('pyquickstart.cli.Project')
    result = CliRunner().invoke(main)

    assert result.exit_code == 0
    assert 'Success!' in result.output
