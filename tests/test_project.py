from pathlib import Path


def test_new_project_from_standard_template_created(project, tmp_path):
    assert project.path == Path(tmp_path) / 'shinyprojectname'


def test_project_defaults_to_standard_template(project):
    assert project.template == 'standard'
