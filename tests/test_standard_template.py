def list_files(path):
    return [f.name for f in path.iterdir() if f.is_file()]


def test_foundation_package_present(project):
    assert len(list_files(project.path / 'src' / 'foundation')) == 1


def test_project_files_present(project):
    assert len(list_files(project.path)) == 9


def test_source_package_present(project):
    assert len(list_files(project.path / 'src')) == 0
