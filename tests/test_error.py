from pytest import raises

from pyquickstart.error import BaseError


def test_baseerror():
    with raises(BaseError):
        raise BaseError()
