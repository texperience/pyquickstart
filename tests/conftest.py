import os

import pytest

from pyquickstart.project import Project


@pytest.fixture
def project(tmp_path):
    os.chdir(tmp_path)
    project = Project()
    project.create(no_prompt=True)

    return project
