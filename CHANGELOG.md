# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog][keepachangelog], and this project adheres to [Semantic Versioning][semver].

## [Unreleased]

## [2.0.0] - 2022-02-15
### Added
* Add support for Python 3.10
* Improve code quality with static code analysis

### Changed
* Upgrade development dependencies
* Enforce maximum line length of 79 reflecting PEP8 suggestion
* Upgrade production dependencies

### Removed
* Remove support for Python 3.6

## [1.2.0] - 2021-03-11
### Added
* Format code and sort imports on tox run

### Changed
* Cleanup code formatting and import style

## [1.1.0] - 2021-02-23
### Added
* Document code contribution guidelines
* Add support for Python 3.9

### Changed
* Rely solely on coverage instead of additional pytest-cov
* Add configuration error to standard template
* Pass posargs to commands in tox environments
* Move imported setup metadata to config
* Switch to source directory structure
* Upgrade development dependencies

### Fixed
* Remove duplicate trove classifier

### Removed
* Delete config module
 
## [1.0.1] - 2020-02-23
### Changed
* Move templates under sources root

### Fixed
* Find package root after deployment

## [1.0.0] - 2020-02-23
### Added
* Implement project creation
* Provide standard project template

[keepachangelog]: https://keepachangelog.com/en/1.0.0/
[semver]: https://semver.org/spec/v2.0.0.html
